/*
 * AC_DC dynniq
 * 14-06-19
*/
// library needs to be added it 
#include <Wire.h>
#include <LiquidCrystal_I2C.h>
#include <Adafruit_INA219.h>
Adafruit_INA219 ina219_A;
Adafruit_INA219 ina219_B(0x41);

// the LCD may have an adress of 0x20 or 0x3F or 0x27
// For this application is de Adress on 0x27Hexa
LiquidCrystal_I2C lcd(0x27,2,1,0,4,5,6,7,3,POSITIVE); 

// datatype
float shuntvoltage_A = 0;
float shuntvoltage_B = 0;
float busvoltage_A = 0;
float busvoltage_B = 0;
float current_A = 0;
float current_B = 0;
float loadvoltage_A = 0;
float power_mW_A = 0;
float power_mW_B = 0;
float loadvoltage_B = 0;
void setup() {
DDRB = 1<<PORTB0 | 1<<PORTB1 | 1<<PORTB2 | 1<<PORTB3;
pinMode(11,OUTPUT);
// Beveiliging is op D11 
// PORTB1 D15 kortsluit
Serial.begin(115200); // uart baudrate
lcd.begin(16,2); // init LCD scherma set backlight high
lcd.setBacklightPin(3,POSITIVE);
lcd.setBacklight(HIGH);


uint32_t currentFrequency;
ina219_A.begin();
ina219_B.begin();
// Initialize the INA219.
// when meausring higher current ina219x.SetCalibration_32_2A();// can be used
// To use a slightly lower 32V, 1A range (higher precision on amps):
ina219_A.setCalibration_32V_1A();
ina219_B.setCalibration_32V_1A();

Serial.println("Measuring voltage and current with INA219 ...");
// init vars
}
void loop()
{
  show_VI(); // show VI format on LCD
  /////////////////////////////////
  // read measure values
  shuntvoltage_A = ina219_A.getShuntVoltage_mV();
  shuntvoltage_B = ina219_B.getShuntVoltage_mV();
  busvoltage_A = ina219_A.getBusVoltage_V();
  busvoltage_B= ina219_B.getBusVoltage_V();
  current_A = ina219_A.getCurrent_mA()/1000;
  current_B = ina219_B.getCurrent_mA()/1000;
  loadvoltage_A = busvoltage_A + (shuntvoltage_A / 1000);
  loadvoltage_B = busvoltage_B + (shuntvoltage_B / 1000);
  ///////////////check for voltage overschoor or short circuit in systeem/////////////////////////////
  if(loadvoltage_A >= 24)
    {
    digitalWrite(11,HIGH);
    Serial.println("Relais aan");
    }
  else if(loadvoltage_A <= 24)
    {
    digitalWrite(11,LOW);
    }
  if(current_B > 2){
    PORTB |= 1<<PORTB1;
    }
  else if(current_B < 2){
    PORTB &= ~(1<<PORTB1);
    }
  ////////////Debug information////////////////
  //Serial.print("Bus Voltage:   "); Serial.print(busvoltage_A); Serial.println(" V");
  //Serial.print("Bus Voltage2:   "); Serial.print(busvoltage_B); Serial.println(" V");
  Serial.print("Shunt Voltage: "); Serial.print(shuntvoltage_A); Serial.println(" mV");
  Serial.print("Load VoltageIN:  "); Serial.print(loadvoltage_A); Serial.println(" V");
  Serial.print("Load VoltageUIT:  "); Serial.print(loadvoltage_B); Serial.println(" V");
  Serial.print("CurrentIN:       "); Serial.print(current_A); Serial.println(" A");
  Serial.print("CurrentUIT:       "); Serial.print(current_B); Serial.println(" A");
  // with Serial.print or Serial.println can a variable be printed to the Serail port
  ///////////Print VI values on the LCD//////////////
  lcd.setCursor(2,0);
  lcd.print(loadvoltage_A,1);
  lcd.setCursor(11,0);
  lcd.print(loadvoltage_B,1);
  
  lcd.setCursor(2,1);
  lcd.print(current_A,2);
  lcd.setCursor(11,1);
  lcd.print(current_B,2);
 ////////////////////////////////////
 //////////////Get power value of the INA/////////////////////
  //power_mW_A = (ina219_A.getPower_mW()/1000);
  //power_mW_B = (ina219_B.getPower_mW()/1000);
  //Serial.print("Power:IN         "); Serial.print(power_mW_A); Serial.println(" W");
  //Serial.print("Power:UIT        "); Serial.print(power_mW_B); Serial.println(" W");
  delay(2000);
  /////////////////check again for overshoot and shortcircuit//////////////////////////
  shuntvoltage_A = ina219_A.getShuntVoltage_mV();
  loadvoltage_A = busvoltage_A + (shuntvoltage_A / 1000);
  if(loadvoltage_A >= 23){
    PORTB |= 1<<PORTB3;
    digitalWrite(11,HIGH);
    Serial.println("Relais aan");
    }
  else if(loadvoltage_A <= 23){
    digitalWrite(11,LOW);
    }
    if(current_B > 2){
    PORTB |= 1<<PORTB1;
    }
    else if(current_B < 2){
    PORTB &= ~(1<<PORTB1);
    }
   ////////////Show Power and efficienty////////////////////////////
  show_P(busvoltage_A,current_A,busvoltage_B,current_B);
}

void show_VI()
{
  lcd.clear();
  lcd.home();
  lcd.print("IN");
  lcd.setCursor(6,0); 
  lcd.print("V");
  
  lcd.setCursor(8,0);
  lcd.print("OUT");
  lcd.setCursor(15,0);
  lcd.print("V");
  
  lcd.setCursor(0,1);
  lcd.print("IN");
  lcd.setCursor(6,1);
  lcd.print("A");

  lcd.setCursor(8,1);
  lcd.print("OUT");
  lcd.setCursor(15,1);
  lcd.print("A");
}

void show_P(float a , float b,float c, float d)
{
  lcd.clear();
  lcd.home();
  float effie = 0;
  lcd.setCursor(0,0);
  lcd.print("PIN");
  float PIN = a*b;
  float POUT = c*d;
  lcd.setCursor(3,0);
  Serial.println(PIN);
  if(PIN > 100 && PIN < 0)
  {
    PIN = 0;
  }
  lcd.print(PIN,2);
  lcd.print("W");
  lcd.setCursor(0,1);
  lcd.print("POUT");
  lcd.setCursor(4,1);
  Serial.println(POUT);
  if(POUT > 100 && POUT < 0)
  {
     POUT = 0;
  }
  lcd.print(POUT,1);
  lcd.print("W");
  lcd.setCursor(10,0);
  lcd.print("n");
  effie = (POUT/PIN)*100;
  if(effie <= 0 || effie > 100){
     effie = 0; 
   }
  Serial.println(effie);
  lcd.setCursor(11,0);
  lcd.print(effie,1);
  lcd.setCursor(15,0);
  lcd.print("%");

  delay(1500);
}
